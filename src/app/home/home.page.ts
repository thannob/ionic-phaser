import { Component } from '@angular/core';
import Phaser from 'phaser';

class GameScene extends Phaser.Scene {
  background;
  mummy;
  anims;
  logo;
  constructor(config) {
      super(config);
  }

  preload() {
    //โหลดรูปทั้ง 3
    this.load.image('logo', 'assets/dit_rsu.png');
    this.load.image('background', 'assets/lazur_skkaay3.png');
    this.load.spritesheet('mummy', 'assets/metalslug_mummy37x45.png',{ frameWidth: 37, frameHeight: 45, endFrame: 18 });
  }

  create() {
    //เพิ่ม background
    this.background = this.add.image(0, window.innerHeight, 'background').setOrigin(0,1);
    this.background.setScale(4.5);
    this.background.smoothed = false;

    //เพิ่ม logo
    this.logo = this.add.image(0,0,'logo').setOrigin(0);

    //เพิ่ม mummy
    this.mummy = this.add.sprite(100, window.innerHeight - 130, 'mummy', 17);
    this.mummy.setScale(3);
    this.mummy.smoothed = false;

    //เพิ่มข้อความ Hello Phaser
    this.add.text(0, 64, 'Hello Phaser');

    //เพิ่ม animation walk ให้ mummy
    var config = {
      key: 'walk',
      frames: this.anims.generateFrameNumbers('mummy', { start: 0, end: 17, first: 17 }),
      frameRate: 20,
      repeat: -1
    };
    this.anims.create(config);
    this.mummy.play('walk');
  }

  update() {
    //เลื่อน background ไปเรื่อยๆ
    if(this.mummy.anims.isPlaying)
      this.background.x -= 1;

    //ถ้ารูป background เลื่อนไปจนสุดจะหยุด animation
    if(this.background.x + this.background.width + window.innerWidth === 0)
      this.mummy.anims.stop();


  }
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  phaserGame: Phaser.Game;
  config: Phaser.Types.Core.GameConfig;

  constructor() {
      this.config = {
          type: Phaser.AUTO,
          width: window.innerWidth,
          height: window.innerHeight,
          physics: {
              default: 'arcade'
          },
          parent: 'gamescene',
          scene: GameScene
      };
  }

  ngOnInit(): void {
      this.phaserGame = new Phaser.Game(this.config);
  }
}
